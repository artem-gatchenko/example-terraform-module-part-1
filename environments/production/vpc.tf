module "vpc" {
  source        = "../../modules/vpc"
  vpc_name      = var.production_vpc_name
  vpc_cidr      = var.production_vpc_cidr
}