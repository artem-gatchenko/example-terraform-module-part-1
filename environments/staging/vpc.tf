module "vpc" {
  source        = "../../modules/vpc"
  vpc_name      = var.staging_vpc_name
  vpc_cidr      = var.staging_vpc_cidr
}