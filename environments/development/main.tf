terraform {
  backend "s3" {}
  required_version = "~> 0.12.9"
}

provider "aws" {
  region  = var.development_aws_region
  version = "~> 2.30"
}
