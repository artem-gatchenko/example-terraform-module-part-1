module "vpc" {
  source        = "../../modules/vpc"
  vpc_name      = var.development_vpc_name
  vpc_cidr      = var.development_vpc_cidr
}