terraform {
  extra_arguments "custom_vars" {
    commands = get_terraform_commands_that_need_vars()
    arguments = [
      "-var-file=${get_terragrunt_dir()}/common.tfvars",
    ]
  }
}

remote_state {
  backend = "s3"
  config = {
    bucket         = "tfstate.eu-west-2.artem"
    key            = "${path_relative_to_include()}/terraform.tfstate"
    region         = "eu-west-2"
    encrypt        = true
    dynamodb_table = "artem-lock-table"
  }
}