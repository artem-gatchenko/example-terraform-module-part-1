### DEVELOPMENT ENVIRONMENT ####

development_aws_region = "us-east-1"
development_vpc_name = "ARTEM-DEVELOPMENT-USE1"
development_vpc_cidr = "192.168.0.0/16"

######### END OF BLOCK #########

##### STAGING ENVIRONMENT ######

staging_aws_region = "us-east-1"
staging_vpc_name = "ARTEM-STAGING-USE1"
staging_vpc_cidr = "192.168.0.0/16"

######### END OF BLOCK #########

#### PRODUCTION ENVIRONMENT ####

production_aws_region = "eu-central-1"
production_vpc_name = "ARTEM-PRODUCTION-EUC1"
production_vpc_cidr = "192.168.0.0/16"

######### END OF BLOCK #########