### DEVELOPMENT ENVIRONMENT ####

variable "development_aws_region" {}
variable "development_vpc_name" {}
variable "development_vpc_cidr" {}

######### END OF BLOCK #########

##### STAGING ENVIRONMENT ######

variable "staging_aws_region" {}
variable "staging_vpc_name" {}
variable "staging_vpc_cidr" {}

######### END OF BLOCK #########

#### PRODUCTION ENVIRONMENT ####

variable "production_aws_region" {}
variable "production_vpc_name" {}
variable "production_vpc_cidr" {}

######### END OF BLOCK #########