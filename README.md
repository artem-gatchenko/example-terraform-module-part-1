![terraform](terraform-logo.png)

# Terraform(Terragrunt) - Module example. Part 1

## EN

This repository belongs to the following article: [Terraform(Terragrunt) - Writting a module. Part 1](https://artem.services/?p=1428&lang=en).

## RU

Данный репозиторий относится к статье: [Terraform(Terragrunt) - Пишем модуль. Часть 1](https://artem.services/?p=1421).